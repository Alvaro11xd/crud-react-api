import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

export const show_alerta = (mensaje, icono, focus='') => {
  onFocus(focus);
  const MySawl = withReactContent(Swal);
  MySawl.fire({
    title: mensaje,
    icon: icono,
  });
};

const onFocus = (foco) => {
  if (foco !== "") {
    document.getElementById(foco).focus();
  }
};
